from .Transicion import Transicion
from .Tokens import Tipo, Token
from .Retroceso import Retroceso

import tkinter as tk
import time
from tkinter import filedialog, messagebox

ruta=""
class Automata():
    
    def __init__(self, ruta_archivo, codigo = ""):
        self.estados = []
        self.inicial = []
        self.alfabeto = []
        self.finales = []
        self.reservadas = [
                "IF",
                "ELSE",
                "THEN",
                "WHILE",
                "PROGRAM",
                "BEGIN",
                "END",
                "PRINT",
                "CONST",
                "STRUCTURE",
                "VARS",
                "WRITE",
                "READ",
                "NULL",
                "INT",
                "CHAR",
                "STRING",
                "POINTER",
                "DO",
        ]

        self.transiciones = []
        self.transicionesObj = []

        self.n_retrocesos = []
        self.n_retrocesosObj = []

        self.tipo_Tokens = []
        self.tipo_TokensObj = []

        self.codigo = codigo
        self.tokens = []
        self.pos = 0
        self.linea = 1

        self.ruta_archivo = ruta_archivo
        self.abrirArchivo()

    def setCodigo(self, codigo):
        self.codigo = codigo
        
    def agregarAutomata(self, subcad):
        valores = subcad[1].split(",")

        if subcad[0] == "Estados":
            self.estados = valores
        elif subcad[0] == "Inicial":
            self.inicial = valores
        elif subcad[0] == "Alfabeto":
            self.alfabeto = valores
        elif subcad[0] == "Finales":
            self.finales = valores
        elif subcad[0] == "Transicion":
            self.arregloTransiciones(subcad[1])
        elif subcad[0] == "Retroceso":
            self.arregloRetrocesos(subcad[1])
        elif subcad[0] == "Token":
            self.arregloTokens(subcad[1])

    def crearTuplas(self, valores):
        valores = valores.split(";")
        return valores

    def arregloTransiciones(self, transicion):
        subcad = transicion.split(",")
        transicionObj = Transicion(subcad[0][1:], subcad[1], subcad[2][0:-1])
        self.transicionesObj.append(transicionObj)

    def arregloTokens(self, token):
        token = token.replace("(", "")
        token = token.replace(")", "")
        subcad = token.split(",")
        tokenObj = Tipo(subcad[0], subcad[1])
        self.tipo_TokensObj.append(tokenObj)

    def arregloRetrocesos(self, retroceso):
        retroceso = retroceso.replace("(", "")
        retroceso = retroceso.replace(")", "")
        subcad = retroceso.split(",")
        retrocesoObj = Retroceso(subcad[0], subcad[1])
        self.n_retrocesosObj.append(retrocesoObj)

    def abrirArchivo(self):
        with open(self.ruta_archivo, "r") as archivo:
            for linea in archivo:
                    cadena = linea.strip()
                    if len(cadena) != 0:
                        cadena = cadena.replace(" ", "")
                        subcad = cadena.split("^")
                        self.agregarAutomata(subcad)

    def procesarCodigo(self):
        self.archivo_lineas = self.codigo.split("\n")
        print(cadena)
    
    def tablaHash(self):
        tabla_hash = {}
        with open("Automatas/reservadas.txt", 'r') as file:
            for linea in file:
                palabra = linea.strip().lower() 
                tabla_hash[palabra] = None
        return tabla_hash
    
    def buscarPalabra(self,tabla_hash, palabra):
            palabra = palabra.lower()  
            if palabra in tabla_hash:
                return 1
            else:
                return 0

    def buscarToken(self):
        cadena = self.codigo[self.pos:] + " "
        aux = ""
        estadoOrigen = self.inicial[0]
        i = 0
        tabla_hash = self.tablaHash()

        while i < len(cadena):
            caracter = cadena[i]
            self.pos += 1

            if caracter not in self.alfabeto and caracter != " " and caracter != "\t" and caracter != "," and caracter != "\n":
                token = Token("Error: Caracter no reconocido", caracter, self.linea)
                return token

            if caracter != " " and caracter != "\t" and caracter != "\n":
                aux = aux + caracter

            if caracter.isdigit():
                caracter = "dig"
            elif caracter == " " or caracter == "\t" or caracter == "\n":
                caracter = "space"
            elif caracter.isalpha():
                caracter = "letra"
            elif caracter == ",":
                caracter = "coma"

            for index, transicion in enumerate(self.transicionesObj):
                estadoNuevo = transicion.comprobar(estadoOrigen, caracter)
                if estadoNuevo != "-1":
                    estadoOrigen = estadoNuevo
                    if estadoNuevo in self.finales:
                        for tipo_token in self.tipo_TokensObj:
                            if estadoNuevo == tipo_token.estado:
                                for retroceso in self.n_retrocesosObj:
                                    ret = retroceso.comprobarRetroceso(estadoNuevo)
                                    if ret != -1: #-1 es un retroceso que no coincide
                                        self.pos = self.pos - ret
                                        if caracter != "space" and ret > 0:
                                            lexema = aux[:-ret]
                                            if lexema in self.reservadas:
                                                tipo = lexema
                                            else:
                                                tipo = tipo_token.tipo
                                            token = Token(tipo, aux[:-ret], self.linea)
                                        else:
                                            lexema = aux
                                            if lexema in self.reservadas:
                                                tipo = lexema
                                            else:
                                                tipo = tipo_token.tipo
                                            token = Token(tipo, aux, self.linea)
                                        return token
                                break #Cierra ciclo for -- tipo_token
                    break #Cierra ciclo for -- transicion
                else:
                    if index == len(self.transicionesObj)-1 and (caracter != "space" or len(aux) > 0):
                        estadoOrigen = self.inicial[0]
                        token = Token("Error: No hay token", aux, self.linea)
                        return token
            if cadena[i] == "\n":
                self.linea += 1
            i = i + 1

        token = Token("$", "$", self.linea)
        return token

    def __str__(self):
        return f"Estados: {self.estados}\nInicial: {self.inicial}\nFinales: {self.finales}\nAlfabeto: {self.alfabeto}\nTransiciones: {self.transiciones}\nNumero de caracteres de retroceso por estado: {self.n_retroceso}\nTipos de token por estado final: {self.tipo_Tokens}\nTransicionesObj: {self.transicionesObj}\nTokensObj: {self.tipo_TokensObj}"

