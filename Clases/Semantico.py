from .Tokens import Token
from .DetenerPrograma import DetenerPrograma

class Semantico():

    def __init__(self):
        self.tabla_simbolos = []

    def ErrorSemantico(self, error, lexema, linea): #Funcion de manejo de errores semanticos
        error = f"Error en '{lexema}': {error}. Linea: {linea}"
        raise DetenerPrograma(error)

    def agregarSimbolo(self, simbolo): #Agrega un simbolo a la tabla de simbolos
        self.tabla_simbolos.append(simbolo)

    def analizarSimbolo(self, token): #Busca si un nuevo simbolo ya existe en la tabla de simbolos
        for simbolo in self.tabla_simbolos:
            if simbolo.getNombre() == token.getLexema() and simbolo.getEstructura() == None:
                self.ErrorSemantico("Variable o constante ya existente", token.getLexema(), token.getLinea())

    def analizarSimboloEstructura(self, token): #Analiza si un simbolo ya existe en los simbolos de una estructura
        nombre_variable = token.getLexema()
        for simbolo in self.tabla_simbolos:
            if simbolo.getEstructura() != None and simbolo.getNombre() == nombre_variable:
                self.ErrorSemantico("Variable de estructura ya existente", token.getLexema(), token.getLinea())

    def analizarExpresion(self, token1, token2): 
        '''
        Analiza expresiones de este tipo
            - variable = valor;
            - variable = valor [+,-,*,/] valor;
        '''

        '''
        --------------------------------------------------------------------------------
        ANALISIS "VARIABLE"
        '''
        simbolo_variable = self.buscarSimbolo(token1)
        if simbolo_variable.getTipoSimbolo() == "Constante": #Verifica que no sea una constante
            self.ErrorSemantico("No puede ser modificada una constante", token1.getLexema(), token1.getLinea())
        '''
        FIN ANALISIS VARIABLE
        --------------------------------------------------------------------------------
        '''

        '''
        --------------------------------------------------------------------------------
        ANALISIS "VALORES"
        '''
        if token2.getTipo() == "Identificador":
            simbolo_valor = self.buscarSimbolo(token2) #Busca que el simbolo exista
            if simbolo_valor.getTipo() != simbolo_variable.getTipo(): #Busca que los tipos sean compatibles
                self.ErrorSemantico("No es compatible con la variable", token2.getLexema(), token2.getLinea())
            if simbolo_valor.getValor() == None: #Busca que tenga un valor para poder ser asignado a la variable
                self.ErrorSemantico("No se ha asignado valor", token2.getLexema(), token2.getLinea())
            self.asignarValor_Variable(token1, simbolo_valor.getValor()) #Se asigna valor a la variable en la tabla de simbolos
        else:
            tipo_valor = self.convertirTipo(token2.getTipo()) #Se convierte el tipo de Numero, Cadena, Caracter a INT, STRING, CHAR
            if tipo_valor != simbolo_variable.getTipo(): #Verifica que sean compatibles los tipos
                self.ErrorSemantico("No es compatible con la variable", token2.getLexema(), token2.getLinea())
            self.asignarValor_Variable(token1, token2.getLexema()) #Se asigna valor a la variable en la tabla de simbolos

        '''
        FIN ANALISIS VALORES
        --------------------------------------------------------------------------------
        '''

    def analizarExpresion_EstructurasDeControl(self, token1, token2): #Analiza la expresion de una estructura de control
        if token1.getTipo() == "Identificador":
            simbolo = self.buscarSimbolo(token1)
            tipo1 = simbolo.getTipo()
            self.analizarValorSimbolos(simbolo, token1)
        else:
            tipo1 = self.convertirTipo(token1.getTipo())
        if token2.getTipo() == "Identificador":
            simbolo = self.buscarSimbolo(token2)
            tipo2 = simbolo.getTipo()
            self.analizarValorSimbolos(simbolo, token2)
        else:
            tipo2 = self.convertirTipo(token2.getTipo())

        if tipo1 != tipo2:
            self.ErrorSemantico("Tipos no compatibles para analizar", token2.getLexema(), token2.getLinea())

    def buscarSimbolo(self, token): #Busca un simbolo en toda la tabla
        nombre = token.getLexema()
        for simbolo in self.tabla_simbolos:
            if simbolo.getNombre() == nombre:
                return simbolo
        self.ErrorSemantico("No fue declarada", token.getLexema(), token.getLinea())

    def buscarSimboloEstructura(self, token): #Busca que una variable pertenezca a una estructura
        nombre = token.getLexema()
        for simbolo in self.tabla_simbolos:
            if simbolo.getNombre() == nombre and simbolo.getEstructura() != None:
                return simbolo
        self.ErrorSemantico("No fue declarada para esta estructura", token.getLexema(), token.getLinea())

    def analizarVar_NoEstructura(self, token): #Analiza que el identificador no sea un campo de estructura Ejemplo: Jugador.nombre existe pero usuario no puede usar solo 'nombre' sino fue definida en variables
        nombre_variable = token.getLexema()
        bandera = False

        for simbolo in self.tabla_simbolos:
            if simbolo.getNombre() == nombre_variable and simbolo.getEstructura() != None:
                bandera = True
            elif simbolo.getNombre() == nombre_variable and simbolo.getEstructura() == None:
                bandera = False
        if bandera:
            self.ErrorSemantico("No fue declarada", token.getLexema(), token.getLinea())

    def buscarEstructura(self, token): #Busca un simbolo de tipo Estructura
        nombre_estructura = token.getLexema()
        for simbolo in self.tabla_simbolos:
            if simbolo.getNombre() == nombre_estructura and simbolo.getTipoSimbolo() == "Estructura":
                return simbolo
        self.ErrorSemantico("Estructura no existente", token.getLexema(), token.getLinea())

    def verificarIdentificador(self, token): #Verifica que el identificador en este tipo de definicion "Identificador variable;" sea una estructura
        simbolo = self.buscarSimbolo(token)
        if simbolo.getTipo() == "INT" or simbolo.getTipo() == "CHAR" or simbolo.getTipo() == "STRING":
            self.ErrorSemantico("Error esta no es una variable de estructura", token.getLexema(), token.getLinea())

    def asignarValor_Variable(self, token, valor): #Solo asigna valor a una variable 'a' en una expresion de este tipo "a = b"
        nombre = token.getLexema()
        for index, simbolo in enumerate(self.tabla_simbolos):
            if nombre == simbolo.getNombre():
                self.tabla_simbolos[index].setValor(valor)
                return None

    def getTipoSimbolo(self, nombre):
        simbolo = self.buscarSimbolo()
        return simbolo.getTipo()

    def analizarValorSimbolos(self, simbolo, token): #Verifica que los simbolos tengan un valor antes de ser usados
        if simbolo.getValor() == None:
            self.ErrorSemantico("No se ha asignado valor", token.getLexema(), token.getLinea())

    def convertirTipo(self, tipo): #Convierte el tipo de un token a tipos de variables
        if tipo == "Numero":
            return "INT"
        elif tipo == "Cadena":
            return "STRING"
        elif tipo == "Caracter":
            return "CHAR"
        else:
            return "Identificador"

    def mostrarTabla(self):
        for simbolo in self.tabla_simbolos:
            print(simbolo)
