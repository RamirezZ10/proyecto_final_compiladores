class DetenerPrograma(Exception):
    def __init__(self, mensaje="Recursión detenida"):
        self.mensaje = mensaje
        super().__init__(self.mensaje)
