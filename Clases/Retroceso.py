class Retroceso():

    def __init__(self, _estado, _retroceso):
        self.estado = _estado
        self.retroceso = _retroceso

    def comprobarRetroceso(self, origen):
        if origen == self.estado:
            return int(self.retroceso)
        else:
            return -1
