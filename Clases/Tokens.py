class Tipo():

    def __init__(self, estado, tipo):
        self.estado = estado
        self.tipo = tipo

class Token():

    def __init__(self, _tipo, _lexema, _linea):
        self.tipo = _tipo
        self.lexema = _lexema
        self.linea = _linea

    def getTipo(self):
        return self.tipo

    def getLexema(self):
        return self.lexema

    def getLinea(self):
        return self.linea
        
    def __str__(self):
        return f"""<{self.tipo}, {self.lexema}, {self.linea}>\n"""
