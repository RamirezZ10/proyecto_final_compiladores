from .Automata import Automata
from .Semantico import Semantico
from .Simbolo import Simbolo
from .Tokens import Token
from .DetenerPrograma import DetenerPrograma

from os import system

class Sintactico():

    def __init__(self, codigo):
        self.analizadorLexico = Automata("/home/yahir/Documentos/9no Semestre/Compiladores/Proyecto_Final/Automatas/automata.txt", codigo)
        self.analizadorSemantico = Semantico()
        self.preanalisis = None
        self.iniciarAnalisis()
        self.analizadorSemantico.mostrarTabla()

    def emparejar(self, terminal):
        if terminal == self.preanalisis.getTipo():
            #print(f"BIEN: {self.preanalisis.getTipo()} -- {self.preanalisis.getLexema()}")
            token = self.preanalisis
            self.preanalisis = self.obtenerSiguienteToken()
            return token
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), terminal, self.preanalisis.getLinea())

    def iniciarAnalisis(self):
        self.preanalisis = self.obtenerSiguienteToken()
        self.Programa()

    def obtenerSiguienteToken(self):
        token = self.analizadorLexico.buscarToken()
        token_validacion = token.getTipo().split(" ")

        if token_validacion[0] == "Error:":
            self.ErrorLexico(token.getLexema(), token.getTipo(), token.getLinea())

        return token

    def ErrorSintactico(self, lexema, expected, linea):
        if lexema == "$":
            lexema = "Fin Archivo"
        error = f"Encontrado '{lexema}'. Se esperaba {expected}. Linea: {linea}"
        raise DetenerPrograma(error)

    def ErrorLexico(self, lexema, error, linea):
        error = f"Encontrado '{lexema}'. {error}. Linea: {linea}"
        raise DetenerPrograma(error)

    def crearSimbolo(self, tipo, nombre, valor, simbolo, estructura = None):
        simbolo = Simbolo(tipo, nombre, valor, simbolo, estructura)
        self.analizadorSemantico.agregarSimbolo(simbolo)

    def Programa(self):
        if self.preanalisis.getTipo() == "PROGRAM":
            self.emparejar("PROGRAM")
            self.emparejar("Identificador")
            self.Definiciones()
            self.emparejar("BEGIN")
            self.Instrucciones()
            self.emparejar("END")
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["PROGRAM"], self.preanalisis.getLinea())

    def Definiciones(self):
        if self.preanalisis.getTipo() == "CONST":
            self.emparejar("CONST")
            self.DefCons()
            self.Estructuras()
        elif self.preanalisis.getTipo() == "STRUCTURE":
            self.Estructuras()
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["CONST", "STRUCTURE"], self.preanalisis.getLinea())

    def Estructuras(self):
        if self.preanalisis.getTipo() == "STRUCTURE":
            self.emparejar("STRUCTURE")
            self.DefEst()
            self.Variables()
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["STRUCTURE"], self.preanalisis.getLinea())

    def Variables(self):
        if self.preanalisis.getTipo() == "VARS":
            self.emparejar("VARS")
            self.ListaDefVarAux()
        elif self.preanalisis.getTipo() == "BEGIN":
            pass
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["VARS", "BEGIN"], self.preanalisis.getLinea())

    def DefCons(self):
        if self.preanalisis.getTipo() == "Identificador":
            self.Constante()
            self.DefConsAux()
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["Identificador"], self.preanalisis.getLinea())

    def DefConsAux(self):
        if self.preanalisis.getTipo() == "Identificador":
            self.Constante()
            self.DefConsAux()
        elif self.preanalisis.getTipo() == "STRUCTURE":
            pass
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["Identificador", "STRUCTURE"], self.preanalisis.getLinea())

    def Constante(self):
        if self.preanalisis.getTipo() == "Identificador":
            token1 = self.emparejar("Identificador")
            self.emparejar("Asignacion")
            token2, tipo = self.valor()
            self.analizadorSemantico.analizarSimbolo(token1)
            self.crearSimbolo(tipo, token1.getLexema(), token2.getLexema(), "Constante")
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["Identificador"], self.preanalisis.getLinea())

    def DefEst(self):
        if self.preanalisis.getTipo() == "Identificador":
            token = self.emparejar("Identificador")
            nombre_estructura = token.getLexema()
            self.crearSimbolo(None, token.getLexema(), None, "Estructura")

            self.emparejar("Asignacion")
            self.emparejar("LlaveA")

            tipo = self.tipo()
            token = self.emparejar("Identificador")
            self.crearSimbolo(tipo, token.getLexema(), None, "Variable", nombre_estructura)

            self.emparejar("PuntoYComa")

            tipo = self.tipo()
            token = self.emparejar("Identificador")
            self.analizadorSemantico.analizarSimboloEstructura(token)
            self.crearSimbolo(tipo, token.getLexema(), None, "Variable", nombre_estructura)

            self.emparejar("PuntoYComa")
            self.emparejar("LlaveC")
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["Identificador"], self.preanalisis.getLinea())

    def ListaDefVarAux(self):
        if self.preanalisis.getTipo() == "CHAR" or self.preanalisis.getTipo() == "INT" or self.preanalisis.getTipo() == "STRING" or self.preanalisis.getTipo() == "Identificador":
            self.DefVar()
            self.ListaDefVarAux()
        elif self.preanalisis.getTipo() == "PuntoYComa":
            self.emparejar("PuntoYComa")
            self.ListaDefVarAux()
        elif self.preanalisis.getTipo() == "BEGIN":
            pass
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["CHAR", "INT", "STRING", "Identificador"], self.preanalisis.getLinea())

    def DefVar(self):
        if self.preanalisis.getTipo() == "CHAR" or self.preanalisis.getTipo() == "INT" or self.preanalisis.getTipo() == "STRING":
            tipo = self.tipo()
            self.ListaVar(tipo)
            self.emparejar("PuntoYComa")
        elif self.preanalisis.getTipo() == "Identificador":
            token = self.emparejar("Identificador")
            self.analizadorSemantico.buscarEstructura(token)
            self.ListaVar(token.getLexema())
            self.emparejar("PuntoYComa")
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["CHAR", "INT", "STRING", "Identificador"], self.preanalisis.getLinea())

    def ListaVar(self, tipo = None):
        if self.preanalisis.getTipo() == "Identificador":
            token = self.emparejar("Identificador")
            self.analizadorSemantico.analizarSimbolo(token)
            self.crearSimbolo(tipo, token.getLexema(), None, "Variable")
            self.ListaVarAux(tipo)
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["Identificador"], self.preanalisis.getLinea())
        
    def ListaVarAux(self, tipo = None):
        if self.preanalisis.getTipo() == "Coma":
            self.emparejar("Coma")
            token = self.emparejar("Identificador")
            self.analizadorSemantico.analizarSimbolo(token)
            self.crearSimbolo(tipo, token.getLexema(), None, "Variable")
            self.ListaVarAux(tipo)
        elif self.preanalisis.getTipo() == "PuntoYComa":
            pass
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), [",", ";"], self.preanalisis.getLinea())

    def Instrucciones(self):
        if self.preanalisis.getTipo() == "WRITE" or self.preanalisis.getTipo() == "WHILE" or self.preanalisis.getTipo() == "READ" or self.preanalisis.getTipo() == "IF" or self.preanalisis.getTipo() == "Identificador":
            self.Instruccion()
            self.InstruccionesAux()
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["WHILE", "IF", "WRITE", "READ", "Identificador"], self.preanalisis.getLinea())

    def InstruccionesAux(self):
        if self.preanalisis.getTipo() == "WRITE" or self.preanalisis.getTipo() == "WHILE" or self.preanalisis.getTipo() == "READ" or self.preanalisis.getTipo() == "IF" or self.preanalisis.getTipo() == "Identificador":
            self.Instruccion()
            self.InstruccionesAux()
        elif self.preanalisis.getTipo() == "END" or self.preanalisis.getTipo() == "ELSE":
            pass
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["WHILE", "IF", "WRITE", "READ", "END", "ELSE", "Identificador"], self.preanalisis.getLinea())

    def Instruccion(self):
        if self.preanalisis.getTipo() == "IF":
            self.CondIF()
        elif self.preanalisis.getTipo() == "WHILE":
            self.SentWhile()
        elif self.preanalisis.getTipo() == "WRITE":
            self.Escribir()
        elif self.preanalisis.getTipo() == "READ":
            self.Leer()
        elif self.preanalisis.getTipo() == "Identificador":
            self.Expresiones()
        elif self.preanalisis.getTipo() == "Identificador":
            self.ManEst()
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["IF", "WRITE", "READ", "WHILE", "CondIF"], self.preanalisis.getLinea())

    def CondIF(self):
        if self.preanalisis.getTipo() == "IF":
            self.emparejar("IF")
            self.emparejar("ParentesisA")
            self.condicion()
            self.emparejar("ParentesisC")
            self.emparejar("THEN")
            self.Instrucciones()
            self.CondIFAux()
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["IF"], self.preanalisis.getLinea())

    def CondIFAux(self):
        if self.preanalisis.getTipo() == "ELSE":
            self.emparejar("ELSE")
            self.Instrucciones()
            self.emparejar("END")
        elif self.preanalisis.getTipo() == "END":
            self.emparejar("END")
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["ELSE", "END"], self.preanalisis.getLinea())

    def condicion(self):
        if self.preanalisis.getTipo() == "Identificador" or self.preanalisis.getTipo() == "Cadena" or self.preanalisis.getTipo() == "Numero" or self.preanalisis.getTipo() == "Caracter" or self.preanalisis.getTipo() == "NULL":
            token1, tipo = self.valor()
            self.Simbolo()
            token2, tipo = self.valor()
            self.analizadorSemantico.analizarExpresion_EstructurasDeControl(token1, token2)
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["Identificador", "Cadena", "Caracter", "Numero", "NULL"], self.preanalisis.getLinea())

    def Simbolo(self):
        if self.preanalisis.getTipo() == "MayorQue":
            self.emparejar("MayorQue")
        elif self.preanalisis.getTipo() == "MenorQue":
            self.emparejar("MenorQue")
        elif self.preanalisis.getTipo() == "MayorOIgual":
            self.emparejar("MayorOIgual")
        elif self.preanalisis.getTipo() == "MenorOIgual":
            self.emparejar("MenorOIgual")
        elif self.preanalisis.getTipo() == "Igualdad":
            self.emparejar("Igualdad")
        elif self.preanalisis.getTipo() == "Diferente":
            self.emparejar("Diferente")
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), [">", "<", ">=", "<=", "==", "!="], self.preanalisis.getLinea())

    def SentWhile(self):
        if self.preanalisis.getTipo() == "WHILE":
            self.emparejar("WHILE")
            self.emparejar("ParentesisA")
            self.condicion()
            self.emparejar("ParentesisC")
            self.emparejar("DO")
            self.Instrucciones()
            self.emparejar("END")
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["WHILE"], self.preanalisis.getLinea())

    def Escribir(self):
        if self.preanalisis.getTipo() == "WRITE":
            self.emparejar("WRITE")
            self.emparejar("ParentesisA")
            token, tipo = self.valor()
            if tipo == "Identificador":
                self.analizadorSemantico.buscarSimbolo(token)
            self.emparejar("ParentesisC")
            self.emparejar("PuntoYComa")
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["WRITE"], self.preanalisis.getLinea())

    def Leer(self):
        if self.preanalisis.getTipo() == "READ":
            self.emparejar("READ")
            self.emparejar("ParentesisA")
            token = self.emparejar("Identificador")
            token = self.ExpEst(token)
            self.emparejar("ParentesisC")
            self.emparejar("PuntoYComa")
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["READ"], self.preanalisis.getLinea())

    def Expresiones(self):
        if self.preanalisis.getTipo() == "Identificador":
            token = self.emparejar("Identificador")
            self.analizadorSemantico.analizarVar_NoEstructura(token)
            token = self.ExpEst(token)
            self.emparejar("Asignacion")
            self.expresion(token)
            self.emparejar("PuntoYComa")
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["Identificador"], self.preanalisis.getLinea())

    def ExpEst(self, token = None):
        if self.preanalisis.getTipo() == "Punto":
            self.analizadorSemantico.verificarIdentificador(token)
            self.emparejar("Punto")
            token = self.emparejar("Identificador")
            self.analizadorSemantico.buscarSimboloEstructura(token)
            return token
        elif self.preanalisis.getTipo() == "Asignacion" or self.preanalisis.getTipo() == "ParentesisC" or self.preanalisis.getTipo() == "MayorQue" or self.preanalisis.getTipo() == "MenorQue" or self.preanalisis.getTipo() == "MayorOIgual" or self.preanalisis.getTipo() == "MenorOIgual" or self.preanalisis.getTipo() == "Igualdad" or self.preanalisis.getTipo() == "Diferente" or self.preanalisis.getTipo() == "PuntoYComa" or self.preanalisis.getTipo() == "Suma" or self.preanalisis.getTipo() == "Resta" or self.preanalisis.getTipo() == "Multiplicacion" or self.preanalisis.getTipo() == "Division":
            self.analizadorSemantico.buscarSimbolo(token)
            return token
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), [".", "=", ")"], self.preanalisis.getLinea())

    def expresion(self, token = None):
        if self.preanalisis.getTipo() == "Identificador" or self.preanalisis.getTipo() == "Numero" or self.preanalisis.getTipo() == "NULL" or self.preanalisis.getTipo() == "Cadena" or self.preanalisis.getTipo() == "Caracter":
            self.valor(token)
            self.expresionPrima(token)
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["Identificador", "Numero", "NULL", "Cadena", "Caracter"], self.preanalisis.getLinea())

    def expresionPrima(self, token = None):
        if self.preanalisis.getTipo() == "Suma" or self.preanalisis.getTipo() == "Resta" or self.preanalisis.getTipo() == "Multiplicacion" or self.preanalisis.getTipo() == "Division":
            self.operador()
            self.valor(token)
        elif self.preanalisis.getTipo() == "PuntoYComa":
            pass
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["+", "-", "*", "/", ";"], self.preanalisis.getLinea())

    def operador(self):
        if self.preanalisis.getTipo() == "Suma":
            self.emparejar("Suma")
        elif self.preanalisis.getTipo() == "Resta":
            self.emparejar("Resta")
        elif self.preanalisis.getTipo() == "Multiplicacion":
            self.emparejar("Multiplicacion")
        elif self.preanalisis.getTipo() == "Division":
            self.emparejar("Division")
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["+", "-", "*", "/"], self.preanalisis.getLinea())

    def valor(self, token_recibido = None):
        if self.preanalisis.getTipo() == "Numero":
            token = self.emparejar("Numero")
            if token_recibido != None:
                self.analizadorSemantico.analizarExpresion(token_recibido, token)
            return token, "INT"
        elif self.preanalisis.getTipo() == "Cadena":
            token = self.emparejar("Cadena")
            if token_recibido != None:
                self.analizadorSemantico.analizarExpresion(token_recibido, token)
            return token, "STRING"
        elif self.preanalisis.getTipo() == "Caracter":
            token = self.emparejar("Caracter")
            if token_recibido != None:
                self.analizadorSemantico.analizarExpresion(token_recibido, token)
            return token, "CHAR"
        elif self.preanalisis.getTipo() == "Identificador":
            token = self.emparejar("Identificador")
            self.analizadorSemantico.analizarVar_NoEstructura(token)
            token = self.ExpEst(token)
            if token_recibido != None:
                self.analizadorSemantico.analizarExpresion(token_recibido, token)
            token = self.ExpEst(token)
            return token, "Identificador"
        elif self.preanalisis.getTipo() == "NULL":
            token = self.emparejar("NULL")
            if token_recibido != None:
                self.analizadorSemantico.analizarExpresion(token_recibido, token)
            return token, "NULL"
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["Numero", "Cadena", "Caracter", "Identificador", "NULL"], self.preanalisis.getLinea())

    def tipo(self):
        if self.preanalisis.getTipo() == "CHAR":
            token = self.emparejar("CHAR")
            return token.getLexema()
        elif self.preanalisis.getTipo() == "STRING":
            token = self.emparejar("STRING")
            return token.getLexema()
        elif self.preanalisis.getTipo() == "INT":
            token = self.emparejar("INT")
            return token.getLexema()
        elif self.preanalisis.getTipo() == "POINTER":
            token = self.emparejar("POINTER")
            return token.getLexema()
        elif self.preanalisis.getTipo() == "Identificador":
            token = self.emparejar("Identificador")
            return token.getLexema()
        else:
            self.ErrorSintactico(self.preanalisis.getLexema(), ["CHAR", "STRING", "INT", "POINTER"], self.preanalisis.getLinea())
