class Simbolo():

    def __init__(self, tipo, nombre, valor, tipo_simbolo, estructura = None):
        self.tipo = tipo
        self.nombre = nombre
        self.valor = valor
        self.tipo_simbolo = tipo_simbolo
        self.estructura = estructura

    def getTipo(self):
        return self.tipo

    def getNombre(self):
        return self.nombre

    def getValor(self):
        return self.valor

    def getEstructura(self):
        return self.estructura

    def getTipoSimbolo(self):
        return self.tipo_simbolo

    def setValor(self, _valor):
        self.valor = _valor


    def __str__(self):
        return f"{self.tipo} -- {self.nombre} -- {self.valor} -- {self.tipo_simbolo} -- {self.estructura}"
