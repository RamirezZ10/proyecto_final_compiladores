class Transicion():

    def __init__(self, estadoA, entrada, estadoB):
        self.estadoA = estadoA
        self.entrada = entrada
        self.estadoB = estadoB

    def getEstadoA(self):
        return self.estadoA

    def getEntrada(self):
        return self.entrada

    def getEstadoB(self):
        return self.estadoB
    
    def comprobar(self, origen, caracter):
        if origen == self.estadoA and caracter == self.entrada:
            return self.estadoB
        else:
            return "-1"
