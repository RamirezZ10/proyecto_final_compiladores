----------------------------------------------------------------------------
Estados ^ estado,estado,estado,...,
Inicial ^ estado
Alfabeto ^ entrada,entrada,entrada,...,
Finales ^ estado,estado,estado,...,

Comentario ^ Hola

Transicion ^ (estadoOrigen,entrada,estadoSiguiente)
Transicion ^ (estadoOrigen,entrada,estadoSiguiente)
Transicion ^ (estadoOrigen,entrada,estadoSiguiente)

Retroceso ^ (estado, n_retroceso)
Token ^ (estado, token)
----------------------------------------------------------------------------

----------------------------------------------------------------------------
        patrones = [
            (r'\"[0-9a-zA-Z\s]*\"', 'Cadena'),
            (r'[a-zA-Z_][a-zA-Z0-9_]*', 'Identificador'),
            (r'\'[a-zA-Z]{1}\'', 'Caracter'),
            (r'[0-9]+\.[0-9]+', 'RealPositivo'),
            (r'[0-9]+', 'EnteroPositivo'),

            (r'==', "Igualdad"),
            (r'<=', "MenorOIgual"),
            (r'>=', "MayorOIgual"),
            (r'!=', "Diferente"),
            (r'<', 'MenorQue'),
            (r'>', 'MayorQue'),

            (r'\+', 'Suma'),
            (r'-', 'Resta'),
            (r'\*', 'Multiplicacion'),
            (r'/', 'Division'),
            (r'=', "Asignacion"),

            (r'\(', 'ParentesisAbre'),
            (r'\)', 'ParentesisCierra'),
            (r';', 'PuntoYComa'),
        ]

----------------------------------------------------------------------------
        palabras_reservadas = [
            "IF", 
            "ELSE", 
            "THEN", 
            "WHILE",
            "PROGRAM",
            "BEGIN",
            "END",
            "PRINT",
            "CONST",
            "STRUCTURE",
            "VARS",
            "WRITE",
            "READ",
        ]
----------------------------------------------------------------------------

