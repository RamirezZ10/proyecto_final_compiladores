PROGRAM EJEMPLO

    STRUCTURE
        Jugador = {INT vidas; STRING nombre;}

    VARS
        Jugador yahir, david;
        INT vidas;

BEGIN
    vidas = 5;
    yahir.vidas = 3;
    david.vidas = yahir.vidas;
END
