from Clases.Automata import Automata
from Clases.Sintactico import Sintactico

import tkinter as tk
import time
from tkinter import filedialog, messagebox

if __name__ == "__main__":
    # Función para abrir un archivo de texto
    def abrir_archivo():
        archivo = filedialog.askopenfilename(filetypes=[("Archivos de texto", "*.txt")])
        if archivo:
            with open(archivo, 'r') as file:
                texto = file.read()
            texto_editable.delete('1.0', tk.END)
            texto_editable.insert(tk.END, texto)
            texto_editable.config(state=tk.NORMAL)  # Habilitar edición
        contenido = texto_editable.get("1.0", "end-1c")  # Obtén todo el contenido

    # Función para cargar el automata
    def cargar_automata():
        global ruta
        archivo = filedialog.askopenfilename(filetypes=[("Archivos de texto", "*.txt")])
        messagebox.showinfo("Ventana de Aviso", "Automata cargado correctamente")
        ruta=archivo
        
    # Función para mostrar texto en la ventana
    def iniciar():
        limpiar_ventana()
        contenido = texto_editable.get("1.0", "end-1c")  # Obtén todo el contenido
        try:
            s = Sintactico(contenido)
            '''
            texto = s.interprete
            texto_derecha.config(state=tk.NORMAL)  # Habilitar edición
            texto_derecha.insert(tk.END, texto)
            texto_derecha.config(state=tk.DISABLED)  # Deshabilitar edición                
            '''
        except Exception as e:
            texto = e.mensaje + "\n"
            texto_abajo_1.config(state=tk.NORMAL)  # Habilitar edición
            texto_abajo_1.insert(tk.END, texto)
            texto_abajo_1.config(state=tk.DISABLED)  # Deshabilitar edición                
            #raise e

        '''
        texto = miautomata.buscarToken()
        texto_derecha.config(state=tk.NORMAL)  # Habilitar edición
        texto_derecha.insert(tk.END, texto)
        texto_derecha.config(state=tk.DISABLED)  # Deshabilitar edición                
        '''
        
    def limpiar_ventana():
        miautomata = Automata("./Automatas/automata.txt")
        texto_editable.config(state=tk.NORMAL)  # Habilitar edición
        #texto_editable.delete('1.0', tk.END)
        texto_derecha.config(state=tk.NORMAL)  # Habilitar edición
        texto_derecha.delete('1.0', tk.END)
        texto_derecha.config(state=tk.DISABLED)  # Deshabilitar edición
        texto_abajo_1.config(state=tk.NORMAL)  # Habilitar edición
        texto_abajo_1.delete('1.0', tk.END)
        texto_abajo_1.config(state=tk.DISABLED)  # Deshabilitar edición

    # Crear la ventana principal
    ventana = tk.Tk()
    ventana.title("Editor de Texto")

    # Marco para los botones (disposición horizontal)
    marco_botones = tk.Frame(ventana)
    marco_botones.grid(row=0, column=0, padx=10, pady=10, columnspan=4)

    boton_abrir = tk.Button(marco_botones, text="Abrir archivo", command=abrir_archivo)
    #boton_cargar = tk.Button(marco_botones, text="Cargar automata", command=cargar_automata)
    boton_mostrar = tk.Button(marco_botones, text="Iniciar", command=iniciar)
    boton_limpiar = tk.Button(marco_botones, text="Limpiar", command=limpiar_ventana)

    boton_abrir.grid(row=0, column=0, padx=10, pady=10)
    #boton_cargar.grid(row=0, column=1, padx=10, pady=10)
    boton_mostrar.grid(row=0, column=2, padx=10, pady=10)
    boton_limpiar.grid(row=0, column=3, padx=10, pady=10)

    # Pantallas de texto
    texto_editable = tk.Text(ventana, wrap=tk.WORD, width=40, height=20)
    texto_editable.grid(row=1, column=0, padx=10, pady=10, columnspan=2, rowspan=1)
    texto_editable.config(state=tk.NORMAL)  # Habilitar edición desde el inicio

    texto_derecha = tk.Text(ventana, wrap=tk.WORD, width=40, height=20)
    texto_derecha.grid(row=1, column=2, padx=10, pady=10, columnspan=2, rowspan=1)
    texto_derecha.config(state=tk.DISABLED)  # Deshabilitar edición en la pantalla superior derecha

    # Pantallas de texto adicionales debajo de las existentes
    texto_abajo_1 = tk.Text(ventana, wrap=tk.WORD, width=85, height=10)
    texto_abajo_1.grid(row=2, column=0, padx=10, pady=10, columnspan=4, rowspan=1)
    texto_abajo_1.config(state=tk.DISABLED)  # Deshabilitar edición en la pantalla inferior izquierda

    # Iniciar la aplicación
    ventana.mainloop()


